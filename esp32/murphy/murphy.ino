#include <ros.h>
#include <ESP32Servo.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/Joy.h>
#include <Adafruit_VL53L0X.h>

#define RANGE_MIN 50
#define RANGE_MAX 1300

#define THROTTLE_AX 0
#define THROTTLE_PIN 12
#define THROTTLE_MIN 4500
#define THROTTLE_MAX 5500
#define THROTTLE_NEUTRAL 5000
#define THROTTLE_ACC 10.0

#define STEER_AX 1
#define STEER_PIN 14
#define STEER_MIN 1000
#define STEER_MAX 2000
#define STEER_CENTER 90
#define STEER_RANGE 90

#define TACHO_PIN 35
#define TACHO_CONSTANT 1.38

#define RECEIVER_BUTTON 25
#define RECEIVER_THROTTLE 26
#define RECEIVER_STEER 27

#define COLLISION_TRIGGER_DISTANCE 500

typedef struct {
  Adafruit_VL53L0X *psensor; // pointer to object
  int id;            // id for the sensor
  int shutdown_pin;  // which pin for shutdown;
  Adafruit_VL53L0X::VL53L0X_Sense_config_t  sensor_config;     // options for how to use the sensor
  sensor_msgs::Range *range_msg;
  ros::Publisher *publisher;
  bool state;
} vl53l0x_t;

Servo steer;
ESP32PWM throttle;
Adafruit_VL53L0X vl53_1, vl53_2, vl53_3 ,vl53_4, vl53_5, vl53_6, vl53_7, vl53_8;

volatile byte ticks;
float cur_speed, cur_throt, cur_steer, ref_speed, ref_angle = 0.0, old_range;
volatile int rpm_time, throttle_time, steer_time, button_time, button_state;
int real_throttle, real_angle;

ros::NodeHandle  nh;

// std_msgs::Float32 debug_msg;
std_msgs::Float32 rpm_msg, angle_msg;
std_msgs::Bool button, collision_msg;
sensor_msgs::Joy joy;
sensor_msgs::Range range_1, range_2, range_3, range_4, range_5, range_6, range_7, range_8;

void joy_cb(const sensor_msgs::Joy& joy_msg){
  ref_speed = joy_msg.axes[THROTTLE_AX];
  ref_angle = joy_msg.axes[STEER_AX];
}

// ros::Publisher debug("/esp32_debug", &debug_msg);
ros::Publisher tacho("/rpm", &rpm_msg), steering("/steer", &angle_msg);
ros::Publisher manual_control("/override", &button), collision_pub("/collision", &collision_msg);
ros::Publisher pub_range_1("/range_1", &range_1), pub_range_2("/range_2", &range_2), pub_range_3("/range_3", &range_3), pub_range_4("/range_4", &range_4);
ros::Publisher pub_range_5("/range_5", &range_5), pub_range_6("/range_6", &range_6), pub_range_7("/range_7", &range_7), pub_range_8("/range_8", &range_8);
//ros::Publisher joy_feedback("/joy_feedback", &feedback);
ros::Subscriber<sensor_msgs::Joy> sub_joy("/joy", joy_cb);

//VL53L0X_SENSE_DEFAULT,
//VL53L0X_SENSE_LONG_RANGE,
//VL53L0X_SENSE_HIGH_SPEED,
//VL53L0X_SENSE_HIGH_ACCURACY

const int N_VL53L0X=8;
vl53l0x_t vl53s[N_VL53L0X] = {
    {
      &vl53_1, 0x30, 23,
      Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_1,
      &pub_range_1
    },
    {
      &vl53_2, 0x31, 18,
      Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_2,
      &pub_range_2
      },
    {
      &vl53_3, 0x32, 5,
      Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_3,
      &pub_range_3
      },
    {
      &vl53_4, 0x33, 17,
     Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_4,
      &pub_range_4
      },
    {
      &vl53_5, 0x34, 16,
      Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_5,
      &pub_range_5
      },
    {
      &vl53_6, 0x35, 4,
      Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_6,
      &pub_range_6
      },
    {
      &vl53_7, 0x36, 0,
      Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_7,
      &pub_range_7,
      false
      },
    {
      &vl53_8, 0x37, 2,
      Adafruit_VL53L0X::VL53L0X_SENSE_HIGH_ACCURACY,
      &range_8,
      &pub_range_8,
      false
      }
};

//====================================================================
// Initialize vl53l0x sensors
//====================================================================
void init_vl53s() {

  bool found_any_sensors = false;
  // Set all shutdown pins low to shutdown sensors
  for (int i = 0; i < N_VL53L0X; i++){
    pinMode(vl53s[i].shutdown_pin, OUTPUT);
    digitalWrite(vl53s[i].shutdown_pin, LOW);
  }
  delay(10);

  for (int i = 0; i < N_VL53L0X; i++) {
    // one by one enable sensors and set their ID
    digitalWrite(vl53s[i].shutdown_pin, HIGH);
    delay(10); // give time to wake up.
    if (vl53s[i].psensor->begin(vl53s[i].id, false, &Wire,
                                  vl53s[i].sensor_config)) {
      found_any_sensors = true;

      vl53s[i].range_msg->radiation_type = sensor_msgs::Range::INFRARED;
      //vl53s[i].range_msg.header.frame_id =  frameid;
      vl53s[i].range_msg->field_of_view = 0;
      vl53s[i].range_msg->min_range = RANGE_MIN;
      vl53s[i].range_msg->max_range = RANGE_MAX;
      vl53s[i].state = true;
      
      //vl53s[i].psensor->startRange();
      vl53s[i].psensor->startRangeContinuous(30); // do 30ms cycle
      nh.advertise(*vl53s[i].publisher);
    }// else {
//      error_msg.data = "failed to start vl53l0x: " + vl53s[i].id;
//      error.publish(&error_msg);
//    }
  }
//  if (!found_any_sensors) {
//      error_msg.data = "no vl53l0x sensors found, stopping";
//      error.publish(&error_msg);
//  }
}

//====================================================================
// initialise servos.
//====================================================================
void init_servos() {
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  
  //throttle.setPeriodHertz(50);    // standard 50 hz servo
  steer.setPeriodHertz(50);    // standard 50 hz servo

  throttle.attachPin(THROTTLE_PIN, 50, 16);
  steer.attach(STEER_PIN, STEER_MIN, STEER_MAX);

  cur_throt = THROTTLE_NEUTRAL;
  throttle.write(cur_throt);
  cur_steer = STEER_CENTER;
  steer.write(cur_steer);
  
  // this delay is of uttermost importance to 'prime' the esc, specifying the neutral position
  delay(1500);
  
//  throttle.attach(THROTTLE_PIN, THROTTLE_MIN, THROTTLE_MAX);

  nh.subscribe(sub_joy);
}

//====================================================================
// ASync read vl53l0x.
//====================================================================
void read_vl53s() {
    
  for (int i = 0; i < N_VL53L0X; i++) {
    if (vl53s[i].state){

//      old_range = vl53s[i].range_msg->range;
      //vl53s[i].range_msg->range = RANGE_MAX;
      vl53s[i].range_msg->range = min(RANGE_MAX, (int) vl53s[i].psensor->readRangeResult());
      vl53s[i].range_msg->header.stamp = nh.now();
      vl53s[i].publisher->publish(vl53s[i].range_msg);
  
      //nh.spinOnce();
      // start a new reading
      //vl53s[i].psensor->startRange();
      
//      if ((old_range-vl53s[i].range_msg->range)/old_range > 0.05 && vl53s[i].range_msg->range < COLLISION_TRIGGER_DISTANCE) {
//        collision();
//      }
    }
  }
}

//====================================================================
// collision detection protocol
//====================================================================
void collision() {
  //stop all action
  throttle.write(THROTTLE_NEUTRAL);
  steer.write(STEER_CENTER);
  
  // throw a flag
  collision_msg.data = true;
  collision_pub.publish(&collision_msg);
  
  // let's try to reset the car by driving backwards until the rear sensor sees something
  old_range = min(RANGE_MAX, (int) vl53s[0].psensor->readRangeResult());
  // start a new reading
  vl53s[0].psensor->startRange();
  
  while(old_range > 500){
    
    // measure speed
    cur_speed = ticks*TACHO_CONSTANT/(millis() - rpm_time);
    rpm_time = millis();
    ticks = 0;

    // adjust throttle (in reverse)
    //cur_throt = throttle.read() + 1.0; // the 1.0 originates from a weird fact that when it writes e.g. 11, the read is 10, so we add a 1 as default
    cur_throt = cur_throt - ref_speed*THROTTLE_ACC*(1-cur_speed/abs(0.5));
    throttle.write(cur_throt);
  
    // reread
    old_range = min(RANGE_MAX, (int) vl53s[0].psensor->readRangeResult());
    vl53s[0].psensor->startRange();
  }
  // seems we are good to go!
  throttle.write(THROTTLE_NEUTRAL);
  steer.write(STEER_CENTER);
  collision_msg.data = false;
  collision_pub.publish(&collision_msg);
}
//====================================================================
// initialise controls.
//====================================================================
void rpm_int()
{
  ticks++;
}

void button_change()
{
   if(digitalRead(RECEIVER_BUTTON)){
    button_time = millis();
  }else{
    button.data = (millis()-button_time)>1;
  }
}

void throttle_change()
{
   if(digitalRead(RECEIVER_THROTTLE)){
    throttle_time = micros();
  }else{
    real_throttle = (micros()-throttle_time);
  }
}
void steer_change()
{
   if(digitalRead(RECEIVER_STEER)){
    steer_time = micros();
  }else{
    real_angle = (micros()-steer_time);
  }
}

void init_controls() {
  ref_speed = 0.0;
  ticks = 0;
  rpm_time = millis();
  button_time = millis();
  button_state = 1;

  pinMode(TACHO_PIN, INPUT);
  attachInterrupt(TACHO_PIN, rpm_int, FALLING);
  
  pinMode(RECEIVER_BUTTON, INPUT);
  attachInterrupt(RECEIVER_BUTTON, button_change, CHANGE);
  pinMode(RECEIVER_THROTTLE, INPUT);
  attachInterrupt(RECEIVER_THROTTLE, throttle_change, CHANGE);
  pinMode(RECEIVER_STEER, INPUT);
  attachInterrupt(RECEIVER_STEER, steer_change, CHANGE);

  nh.advertise(tacho);
  nh.advertise(manual_control);
  nh.advertise(collision_pub);
  nh.advertise(steering);

  // no collision!
  collision_msg.data = false;
  collision_pub.publish(&collision_msg);
}

//====================================================================
// adjust controls.
//====================================================================
void adjust_controls() {
  // measure speed
  cur_speed = ticks*TACHO_CONSTANT/(millis() - rpm_time);
  rpm_time = millis();
  ticks = 0;
  rpm_msg.data = cur_speed;

  if (button.data){
    // override mode, read from receiver
    // write to motors
    cur_throt = 3.2768*real_throttle; //hooray for arbitrary constants (usecs to ticks conversion)
    cur_steer = 180.0*(real_angle-STEER_MIN)/(STEER_MAX-STEER_MIN);

    // transform to readable format
    //feedback.axes[THROTTLE_AX] = rpm_msg.data;
    angle_msg.data = 1.0*(real_angle-STEER_MIN)/(STEER_MAX-STEER_MIN)*2.0 - 1.0;
    
  }else{
    // adjust speed
    if (ref_speed == 0.0){
      cur_throt = THROTTLE_NEUTRAL;
    }else{
      
      //cur_throt = throttle.read() + 1.0; // the 1.0 originates from a weird fact that when it writes e.g. 11, the read is 10, so we add a 1 as default

      //throttle.write(cur_throt + ref_speed*THROTTLE_ACC*(1-cur_speed/abs(ref_speed)));
      //double x = 1- cur_speed/abs(ref_speed);
      //throttle.write(cur_throt + THROTTLE_ACC*((exp(x) - 1/x) / (exp(x) + 1/x)));
      if (cur_speed/abs(ref_speed) < 1) {
        cur_throt = cur_throt + ref_speed/abs(ref_speed)*max(1.0, THROTTLE_ACC*pow(1-cur_speed/abs(ref_speed),2));
      }else{
        cur_throt = cur_throt + ref_speed/abs(ref_speed)*min(-1.0, THROTTLE_ACC*pow(1-cur_speed/abs(ref_speed),2));
      }
    }

    // adjust steer  
    cur_steer = STEER_CENTER+ref_angle*STEER_RANGE;
    angle_msg.data = ref_angle;
  }
  
  throttle.write(cur_throt);
  steer.write(cur_steer);
  
  tacho.publish(&rpm_msg);
  manual_control.publish(&button);
  steering.publish(&angle_msg);
  nh.spinOnce();
  
}

void setup()
{
  nh.getHardware()->setBaud(500000);
  nh.initNode();
  //nh.getHardware()->setBaud(921600);
//   nh.advertise(debug);
  init_vl53s();
  init_servos();
  init_controls();
}

void loop()
{
  read_vl53s();
  adjust_controls();
  //nh.spinOnce();
  delay(1);
}
