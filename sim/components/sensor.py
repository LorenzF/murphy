import math

import pygame
import numpy as np
import bezier

from .pos import Pos
from config import Config


class Distance:

    MAX_DISTANCE = 1300
    DEBUG = False

    def __init__(self, car=None, angle=0):
        self.car = car
        self.ang = angle
        self.distance = self.MAX_DISTANCE

    def measure(self):

        end = Pos((
            self.car.cog.x + math.cos(self.car.yaw + self.ang)*self.MAX_DISTANCE,
            self.car.cog.y - math.sin(self.car.yaw + self.ang)*self.MAX_DISTANCE,
        ))
        line = bezier.Curve(
            np.array([[self.car.cog.x, end.x], [self.car.cog.y, end.y]]),
            degree=1)

        distance = 1.0
        for idx, segment in enumerate(self.car.environment):
            try:
                collision = np.hstack((line.intersect(segment[1]), line.intersect(segment[2])))
            except ValueError:
                collision = np.array([[],[]])
            if collision.size > 0:
                distance = collision[0].min()

                if idx==2:
                    self.car.shift_segments()

                break

        if self.DEBUG:
            point = Pos(line.evaluate(distance))
            pygame.draw.line(
                self.car.world.display,
                Config.GREEN,
                self.car.world.screen_coordinates(self.car.cog),
                self.car.world.screen_coordinates(point),
                max(1, int(5 * self.car.world.scale))
            )
            pygame.draw.circle(self.car.world.display, Config.GREEN, self.car.world.screen_coordinates(point),
                               max(1, int(10 * self.car.world.scale)))

        return distance*self.MAX_DISTANCE

    # def detect(self, segment_line=(Pos(0, 0), Pos(1, 1)), world=None):
    #     omega = radians(-self.car.rot + self.ang)
    #     Ax1 = self.car.x
    #     Ay1 = self.car.y
    #     Ax2 = self.car.x + MAX_DISTANCE*sin(omega)
    #     Ay2 = self.car.y + MAX_DISTANCE*cos(omega)
    #     Bx1 = segment_line[0].x
    #     By1 = segment_line[0].y
    #     Bx2 = segment_line[1].x
    #     By2 = segment_line[1].y
    #     # sensor_line = (
    #     #     self.car,
    #     #     vect2d(self.car.x + MAX_DISTANCE*sin(omega), self.car.y + MAX_DISTANCE*cos(omega))
    #     # )
    #     self.distance = 0
    #
    #     d = (By2 - By1) * (Ax2 - Ax1) - (Bx2 - Bx1) * (Ay2 - Ay1)
    #     if d:
    #         uA = ((Bx2 - Bx1) * (Ay1 - By1) - (By2 - By1) * (Ax1 - Bx1)) / d
    #         uB = ((Ax2 - Ax1) * (Ay1 - By1) - (Ay2 - Ay1) * (Ax1 - Bx1)) / d
    #     else:
    #         return
    #     if not (0 <= uA <= 1 and 0 <= uB <= 1):
    #         return
    #     dx = uA * (Ax2 - Ax1)
    #     dy = uA * (Ay2 - Ay1)
    #
    #     self.distance = max(0, 1-((dx)**2 + (dx)**2)**0.5/MAX_DISTANCE)
    #     if CAR_DBG:
    #         py.draw.lines(world.win, GREEN, False, [world.getScreenCoords(self.car.x, self.car.y),
    #                                                     world.getScreenCoords(self.car.x + dx, self.car.y + dy)], 2)
    #         py.draw.circle(world.win, RED, world.getScreenCoords(self.car.x + dx, self.car.y + dy), 6)
    #
    #     return Pos(dx, dy)