from collections import deque
import math
import random

import pygame
import bezier
import numpy as np

from .pos import Pos
from .finish import FinishLine
from config import Config


class Road:

    DEBUG = False

    def __init__(self, world):
        self.world = world
        # self.segment = namedtuple('segment', ['ref', 'left', 'right'])
        self._segments = deque()
        self.segment_length = 5000
        self.ref_width = 500
        self.road_width = self.ref_width
        self.road_barrier_thickness = 5
        self.road_sigma = 0.3
        self.max_turn = math.pi/2
        self.turn_factor = 0.3
        self.start_pos = Pos((0, 0))
        # self.finish = pygame.sprite.Group()
        # self.finish.add(finish_line(self))
        self.finish = None

        # create the first section, a straight piece

        self._segments.append(
            (
                self.start_pos,
                bezier.Curve(
                    np.asfortranarray(
                        [
                            [self.start_pos.x-2000.0, self.start_pos.x-2000.0, self.start_pos.x-1000.0, self.start_pos.x],
                            [self.start_pos.y, self.start_pos.y-self.road_width, self.start_pos.y-self.road_width, self.start_pos.y-self.road_width],
                        ]),
                    degree=3),
                bezier.Curve(
                    np.asfortranarray(
                        [
                            [self.start_pos.x-2000.0, self.start_pos.x-2000.0, self.start_pos.x-1000.0, self.start_pos.x],
                            [self.start_pos.y, self.start_pos.y+self.road_width, self.start_pos.y+self.road_width, self.start_pos.y+self.road_width],
                        ]),
                    degree=3)
            )
        )

        # for i in range(100):
        #     self.add_segment()
        #     #
    @property
    def segments(self):
        return self._segments

    def add_segment(self):

        if self.finish:
            return

        self._segments[-1][1].nodes[:, [-1]] * 2 - self._segments[-1][1].nodes[:, [-2]]
        self.road_width = self.ref_width * 0.2 + self.road_width * 0.8 * random.gauss(1, self.road_sigma)

        added = False
        to_be_popped = 0
        while not added:
            for attempt in range(5):
                starting_point = self._segments[-1][0]
                new_segment = self.calculate_segment(starting_point)
                intersect = False

                for side in [1, 2]:
                    for segment in list(self._segments)[:-1]:
                        overlap_left = new_segment[side].intersect(segment[1])
                        overlap_right = new_segment[side].intersect(segment[2])
                        if overlap_left.size + overlap_right.size > 0:
                            intersect = True

                if not intersect:
                    self._segments.append(new_segment)
                    added = True
                    break
                else:
                    to_be_popped+=1
                    for pop in range(to_be_popped):
                        self._segments.pop()
                    for pop in range(to_be_popped):
                        self.add_segment()

    def calculate_segment(self, starting_point):
        # first nodes
        left_nodes = self._segments[-1][1].nodes[:, [-1]]
        right_nodes = self._segments[-1][2].nodes[:, [-1]]


        angle = math.atan2(left_nodes[1][0] - starting_point.y, left_nodes[0][0] - starting_point.x) + math.pi/2
        #turn = random.choice([math.pi/6,-math.pi/6])
        #turn = random.gauss(0, self.curve_factor)
        turn = random.gauss(0, self.max_turn/3)
        theta = angle + turn
        phi = 2*theta - angle# + random.gauss(0, 0.1)

        # calculating the new reference point
        new_ref_point = Pos((
            starting_point.x + self.segment_length*math.cos(theta),
            starting_point.y + self.segment_length*math.sin(theta),
        ))

        # second nodes
        left_nodes = np.hstack(
            (
                left_nodes,
                #self._segments[-1][1].nodes[:, [-1]] * 2 - self._segments[-1][1].nodes[:, [-2]]
                np.array([
                    [
                        left_nodes[0][0] + math.cos(angle) * self.segment_length * self.turn_factor * (1 + turn / math.pi)
                    ],
                    [
                        left_nodes[1][0] + math.sin(angle) * self.segment_length * self.turn_factor * (1 + turn / math.pi)
                    ],
                ])

            )
        )
        right_nodes = np.hstack(
            (
                right_nodes,
                np.array([
                    [
                        right_nodes[0][0] + math.cos(angle) * self.segment_length * self.turn_factor * (1 - turn / math.pi)
                    ],
                    [
                        right_nodes[1][0] + math.sin(angle) * self.segment_length * self.turn_factor * (1 - turn / math.pi)
                    ],
                ])
            )
        )


        # calculating the last nodes
        last_left_node = np.array([
            [
                new_ref_point.x + self.road_width * math.cos(phi - math.pi / 2),
            ],
            [
                new_ref_point.y + self.road_width * math.sin(phi - math.pi / 2),
            ],
        ])
        last_right_node = np.array([
            [
                new_ref_point.x + self.road_width * math.cos(phi + math.pi / 2),
            ],
            [
                new_ref_point.y + self.road_width * math.sin(phi + math.pi / 2),
            ],
        ])

        # adding the third node
        left_nodes = np.hstack(
            (
                left_nodes,
                np.array([
                    [
                        last_left_node[0][0] + math.cos(phi - math.pi) * self.segment_length * self.turn_factor * (1 + turn / math.pi)
                    ],
                    [
                        last_left_node[1][0] + math.sin(phi - math.pi) * self.segment_length * self.turn_factor * (1 + turn / math.pi)
                    ],
                ])
            )
        )
        right_nodes = np.hstack(
            (
                right_nodes,
                np.array([
                    [
                        last_right_node[0][0] + math.cos(phi - math.pi) * self.segment_length * self.turn_factor * (1 - turn / math.pi)
                    ],
                    [
                        last_right_node[1][0] + math.sin(phi - math.pi) * self.segment_length * self.turn_factor * (1 - turn / math.pi)
                    ],
                ])
            )
        )

        # adding the last node
        left_nodes = np.hstack(
            (
                left_nodes,
                last_left_node,
            )
        )
        right_nodes = np.hstack(
            (
                right_nodes,
                last_right_node,
            )
        )

        return (
            new_ref_point,
            bezier.Curve(
                left_nodes,
                degree=3),
            bezier.Curve(
                right_nodes,
                degree=3)
        )

    def add_finish(self):

        if not self.finish:
            self.finish = FinishLine(self)
            #self.finish.set()
            # center, left_bezier, right_bezier = self._segments[-1]
            # angle = math.atan2(left_bezier.nodes[1, 3] - center.y, left_bezier.nodes[0, 3] - center.x) + math.pi/2
            #
            # self.finish.loc = center
            # self.finish.line = bezier.Curve(
            #     np.hstack((left_bezier.nodes[:, [3]], right_bezier.nodes[:, [3]])),
            #     degree=1,
            # )
            # self.finish.rotate(-angle*180/math.pi)

    def draw(self):
        for ref, left, right in self._segments:
            points = left.evaluate_multi(np.linspace(0, 1, 40))
            points = [self.world.screen_coordinates(Pos(point)) for point in zip(points[0], points[1])]
            pygame.draw.lines(self.world.display, Config.WHITE, False, points, max(1, int(self.road_barrier_thickness * self.world.scale)))
            points = right.evaluate_multi(np.linspace(0, 1, 40))
            points = [self.world.screen_coordinates(Pos(point)) for point in zip(points[0], points[1])]
            pygame.draw.lines(self.world.display, Config.WHITE, False, points, max(1, int(self.road_barrier_thickness * self.world.scale)))

            if self.DEBUG:
                for point in zip(left.nodes[0], left.nodes[1]):
                    pygame.draw.circle(self.world.display, Config.RED, self.world.screen_coordinates(Pos(point)), max(1, int(10*self.world.scale)))
                for point in zip(right.nodes[0], right.nodes[1]):
                    pygame.draw.circle(self.world.display, Config.RED, self.world.screen_coordinates(Pos(point)), max(1, int(10*self.world.scale)))
                pygame.draw.line(
                    self.world.display,
                    Config.RED,
                    self.world.screen_coordinates(Pos(left.nodes[:, 0])),
                    self.world.screen_coordinates(Pos(left.nodes[:, 1])),
                    max(1, int(5*self.world.scale))
                )
                pygame.draw.line(
                    self.world.display,
                    Config.RED,
                    self.world.screen_coordinates(Pos(left.nodes[:, 2])),
                    self.world.screen_coordinates(Pos(left.nodes[:, 3])),
                    max(1, int(5*self.world.scale))
                )
                pygame.draw.line(
                    self.world.display,
                    Config.RED,
                    self.world.screen_coordinates(Pos(right.nodes[:, 0])),
                    self.world.screen_coordinates(Pos(right.nodes[:, 1])),
                    max(1, int(5*self.world.scale))
                )
                pygame.draw.line(
                    self.world.display,
                    Config.RED,
                    self.world.screen_coordinates(Pos(right.nodes[:, 2])),
                    self.world.screen_coordinates(Pos(right.nodes[:, 3])),
                    max(1, int(5*self.world.scale))
                )

        if self.finish:

            new_rect = self.finish.img.get_rect(center=self.finish.loc.tup())
            scaled_img = pygame.transform.scale(
                self.finish.img,
                (
                    int(self.finish.img.get_width() * self.world.scale),
                    int(self.finish.img.get_height() * self.world.scale)
                )
            )
            self.world.display.blit(scaled_img, self.world.screen_coordinates(Pos(new_rect.topleft)))

