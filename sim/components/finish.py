import math

import pygame

from .pos import Pos


class FinishLine:

    def __init__(self, road):
        super().__init__()
        self.road = road

        center, left_bezier, right_bezier = road._segments[-1]
        angle = math.atan2(left_bezier.nodes[1, 3] - center.y, left_bezier.nodes[0, 3] - center.x) + math.pi/2

        self._left = Pos(left_bezier.nodes[:, 3])
        self._loc = center
        self._right = Pos(right_bezier.nodes[:, 3])
        self._width = self._left.distance(self._right)

        self.img = pygame.transform.rotate(
            pygame.transform.scale(
                pygame.image.load(
                    './imgs/finish.png'
                ),
                (
                    int(150 * 1.5),
                    int(900 * 1.5)
                )
            ),
            -angle*180/math.pi,
        )

    @property
    def loc(self):
        return self._loc

    @loc.setter
    def loc(self, loc):
        self._loc = loc

    def crossed(self, pos):
        return (pos.distance(self._left) + pos.distance(self._right)) < self._width * 1.1

    # def update(self):
    #
    #     # prep the image
    #     self.image = pygame.transform.scale(
    #         self.img,
    #         (
    #             int(self.img.get_width()*self.road.world.scale),
    #             int(self.img.get_height()*self.road.world.scale)
    #         )
    #     )
    #     self.rect = self.image.get_rect(center=self.road.world.screen_coordinates(self.loc))