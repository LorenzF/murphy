import os
import time

import pygame

from sim.components import World, Road, Car
from .config import Config

pygame.init()

scale = 0.1


def sim():
    global scale

    world = World((600, 750), scale)
    #world.win.blit(bg, (0,0))
    world.road = Road(world)

    world.scale = 0.01
    world.update()
    n_tracks = 2
    for i in range(n_tracks):
        time.sleep(2/n_tracks)
        world.road.add_segment()
        world.update()

   # world.road.add_finish()
    world.update()

    while world.scale < 0.1:
        time.sleep(0.01)
        world.scale *= 1.05
        world.update()

    car = Car(
        0,
        None,
        None,
        (0, 0,),
        world,
    )
    world.cars.add(car)

    t = 0
    run = True
    world.reset_time()
    while run:
        t += 1
        world.clock.tick(Config.FPS)

        # car.commands['vel'] = car.vel/car.MAX_VEL
        # car.commands['steer'] = 0.0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 4:
                    scale = world.scale = world.scale*1.1
                elif event.button == 5:
                    scale = world.scale = world.scale*0.9

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w:
                    car.commands['speed'] = car.MAX_SPEED
                elif event.key == pygame.K_s:
                    car.commands['speed'] = -car.MAX_SPEED
                elif event.key == pygame.K_a:
                    car.commands['steer'] = car.MAX_ANG
                elif event.key == pygame.K_d:
                    car.commands['steer'] = -car.MAX_ANG

            if event.type == pygame.KEYUP:
                if event.key in [pygame.K_w, pygame.K_s]:
                    car.commands['speed'] = car.speed/car.MAX_SPEED
                if event.key in [pygame.K_a, pygame.K_d]:
                    car.commands['steer'] = 0.0

        world.update()


if __name__ == "__main__":


    sim()
    #print(winner)