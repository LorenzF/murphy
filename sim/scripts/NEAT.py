import os
import time

import pygame
import neat

from sim.components import World, Road, Car, NeatCar
from .config import Config

pygame.init()

# Pos = namedtuple('position', ['x', 'y', 'angle'])

GEN = 0
scale = 0.1


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def neat_sim(genomes=[], config=[]):
    global GEN, scale
    GEN += 1
    print(GEN)

    world = World((600, 750), scale)
    #world.win.blit(bg, (0,0))
    world.road = Road(world)

    world.scale = 0.01
    world.update()
    n_tracks = 2+int(GEN/2)
    for i in range(n_tracks):
        time.sleep(2/n_tracks)
        world.road.add_segment()
        world.update()

    world.road.add_finish()
    world.update()

    while world.scale < 0.1:
        time.sleep(0.01)
        world.scale *=1.05
        world.update()

    carsLeft = True
    cars = iter(sorted(genomes, key=lambda x: x[1].fitness or 0, reverse=True))
    #for chunk in chunks(genomes, 100):
    while (len(world.cars) < Config.N_CARS) & carsLeft:
        try:
            i, g = next(cars)
            world.cars.add(
                NeatCar(
                    i,
                    (0, 0,),
                    world,
                    g,
                    neat.nn.FeedForwardNetwork.create(g, config),
                )
            )
        except StopIteration:
            carsLeft = False


    t = 0
    run = True
    world.reset_time()
    while run:
        t += 1
        world.clock.tick(Config.FPS)
        #world.updateScore(0)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 4:
                    scale = world.scale = world.scale*1.1
                elif event.button == 5:
                    scale = world.scale = world.scale*0.9

        if len(world.cars) < Config.N_CARS and carsLeft:
            try:
                i, g = next(cars)
                world.cars.add(
                    NeatCar(
                        i,
                        (0, 0,),
                        world,
                        g,
                        neat.nn.FeedForwardNetwork.create(g, config),
                    )
                )
            except StopIteration:
                carsLeft = False

        if len(world.cars) == 0:
            run = False
            break

        # if len(world._leaderboard):
        #     if (world._time > world._leaderboard[0][1]+10000):
        #         run = False
        #         world.cars.empty()
        #         break

        world.update()


if __name__ == "__main__":
    config = neat.config.Config(
        neat.DefaultGenome,
        neat.DefaultReproduction,
        neat.DefaultSpeciesSet,
        neat.DefaultStagnation,
        './neat.cfg'
    )

    checkpoints = [ f for f in os.listdir('./checkpoints') if f.startswith('neat')]

    if len(checkpoints)>0:
        last_checkpoint = max([int(f.split('-')[-1]) for f in checkpoints])
        p = neat.checkpoint.Checkpointer.restore_checkpoint(f'./checkpoints/neat-gen-{last_checkpoint}')
    else:
        p = neat.Population(config)

    p.config = config
    GEN = p.generation
    p.add_reporter(neat.StdOutReporter(True))
    stats =neat.StatisticsReporter()
    p.add_reporter(stats)
    checkpointer = neat.Checkpointer(10, 10000, filename_prefix='./checkpoints/neat-gen-')
    p.add_reporter(checkpointer)

    winner = p.run(neat_sim)
    print(winner)