from PIL import Image

img, *imgs = [Image.open(f'./screenshots/screen_{x:04d}.jpg') for x in range(1,146)]
img.save(fp='./vid.gif', format='GIF', append_images=imgs, save_all=True, fps=30, loop=0)